package com.infosys.learning.service;

import com.infosys.learning.dto.Person;
import com.infosys.learning.model.User;
import com.infosys.learning.repository.UserRepository;
import com.infosys.learning.repository.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LearningService {

    @Autowired
    private UserRepository userRepository;



    public String getName(String gender){
        Person person = new Person();
        if ("man".equals(gender))  {
            person.setName("John");
        }else{
            person.setName("Jane");
        }
        return person.getName();
    }

    public Person getNameV2(String gender){
        Person person = new Person();

        if("man".equals(gender)){
            person.setName("John");
        }else{
            person.setName("Jane");
        }

        return person;
    }

    public String getNameV3(String name){
        String fullName = "not identified";

        if ("John".equals(name)){
            fullName = "John Doe";
        }else if ("Jane".equals(name)){
            fullName = "Jane Done";
        }
        return fullName;
    }

    public Integer tahunLahir(Integer tahunLahir)
    {
        Person person = new Person();
        Integer umur;
        String salah;

        if(tahunLahir<=2021) {
            umur = 2021 - tahunLahir;
        }
        else{
            umur = 0;
        }
        return umur;
    }

    public String register(UserRequest userRequest) {
        User existUser = userRepository.findByUserName(userRequest.getUsername());
        if(existUser != null){
            return "Register failed, username is already exist";
        }
        User user  = new User();
        user.setUserName(userRequest.getUsername());
        user.setPassWord(userRequest.getPassword());
        userRepository.save(user);

        return "Register Succes !";
    }

    public String login(UserRequest userRequest){
        //User users = (User) userRepository.findAll();
        User users = userRepository.findByUserName(userRequest.getUsername());
        //User passw = userRepository.findByPassWord(userRequest.getPassword());

        if(users != null ){
            return "Login Failed";
        }

      //  UserRequest userRequest = new User();
        String a = userRequest.getUsername();
        String b = userRequest.getPassword();

        return "Register Succes ! with username a="+a+" password="+b;



            /*if(users.equals(userRequest)){ //contoh lain

               String username = userRequest.getUsername();
               String password = userRequest.getPassword();

                System.out.println("username = "+username+" password = "+password);
                return "username = "+username+" password = "+password;
            }

        System.out.println("username = "+userRequest.getUsername()+" password = "+userRequest.getPassword());
        return "Gagal Login";*/
    }



}
