package com.infosys.learning.dto;

public class Person {
    private String name;
    private Integer angka;


    public Person() {

    }
    public String getName(){
        return name;
    }
    public void setName(String name){

        this.name = name;
    }

    public Integer getAngka() {
        return angka;
    }

    public void setAngka(Integer angka) {
        this.angka = angka;
    }
}
