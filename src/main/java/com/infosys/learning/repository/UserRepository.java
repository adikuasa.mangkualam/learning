package com.infosys.learning.repository;

import com.infosys.learning.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;


    @Repository
    public interface UserRepository extends JpaRepository<User, Integer> {
        User findByUserName(String username);
        User findByPassWord(String password);
    }







